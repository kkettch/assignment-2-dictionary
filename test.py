import subprocess

test_data = [
    {"input_data": "first", "expected_output": "first word text\n"},
    {"input_data": "second", "expected_output": "second word text\n"},
    {"input_data": "third", "expected_output": "third word text\n"},
]

for test_case in test_data:
    process = subprocess.Popen(['./program'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

    input_data = test_case["input_data"]
    expected_output = test_case["expected_output"]

    process.stdin.write(input_data + '\n')
    process.stdin.flush()

    output, error = process.communicate()

    if output == expected_output:
        print("Test with data '{}' passed: expected output received".format(input_data))
    else:
        print("Test with data '{}' failed: unexpected output".format(input_data))
        print("Expected output:")
        print(expected_output)
        print("Received output:")
        print(output)


