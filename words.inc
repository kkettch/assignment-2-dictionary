%include "colon.inc"
section .rodata

colon "third", third_word
db "third word text", 0

colon "second", second_word
db "second word text", 0 

colon "first", first_word
db "first word text", 0 

%define DICT_START_POINT first_word
