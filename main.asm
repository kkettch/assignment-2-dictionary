%include "words.inc"
%include "lib.inc"
%include "dict.inc"

%define SIZE 256
%define BYTE 8

section .bss 
	read_buffer: resb SIZE 		;указываем буфер с вместимостью 255 символов 
section .rodata
        err_too_long_message db "Error in read_word function. Word is too long"
        err_no_matches_message db "There are no matches in find_word function"	
section .text
global _start
_start:
	mov rdi, read_buffer 		;передаем адрес начала буфера
	mov rsi, SIZE 			;передаем размер буфера
	call read_word 			;считываем слово 
	test rax, rax 			;проверяем полученное значение на ноль
	jz .err_too_long		;если ноль, то считывание не удалось - выходим
	mov rdi, read_buffer	 	;передаем считанную нуль-терминированную строку как параметр для функции
	mov rsi, DICT_START_POINT 	;передаем указатель на начало словаря 
	call find_word 			;ищем совпадения строки в словарe
	test rax, rax 			;проверяем результат нахождения на ноль 
	jz .err_no_matches 		;если ноль, то совпадений найдено не было - выходим
	mov rdi, rax 			;адрес начала вхождения в словарь в rdi
	add rdi, BYTE			;переходим к значению связанным с ключом 
	push rdi
	call string_length 		;находим длину строки для ее вывода 
	pop rdi 
	add rdi, rax 			;rdi указывает на символ следующий за найденной строко
	inc rdi				;перемещение к следующему символу после найденной строки 
	call print_string 		;вывод найденной строки в stdout 
	call print_newline 		;переход на следующую строку для красоты вывода
	xor rdi, rdi
	call exit
.err_too_long:
	mov rdi, err_too_long_message
	jmp .error_exit
.err_no_matches:
	mov rdi, err_no_matches_message
	jmp .error_exit
.error_exit:
	call print_error
	call print_newline
	call exit
