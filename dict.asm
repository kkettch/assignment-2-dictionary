%include "lib.inc"
global find_word
section .text

%define byte 8
;rdi - указатель на нуль-терминированную строку.
;rsi - указатель на начало словаря.
;find_word пройдёт по всему словарю в поисках подходящего ключа. 
;Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (не значения)
;Иначе вернёт 0.

find_word:
        push r12
        push r13        	;сохраняем caller-saved регистры для хранения в них rdi и rsi при вызове функции
.loop:
        test rsi, rsi  	 	;сравниваем значение в словаре с нулем 
        jz .no_matches  	;если ноль, то выходим и возвращаем ноль
        mov r12, rdi		
	mov r13, rsi		
	add rsi, byte		;переход к значению в словаре
        call string_equals	;проверка строк на равенство
        mov rdi, r12		;
	mov rsi, r13		;восстановление регистров rsi и rdi после вызова функции
	cmp rax, 1      	;результат функции сравниваем с 1
        jz .match       	;если равно, то совпадения найдены, переходим на завершение работы
        mov rsi, [rsi]  	;записываем в rsi адрес следующего элемента словаря 
        jmp .loop
.no_matches:
        xor rax, rax
        jmp .finish
.match:
        mov rax, rsi
        jmp .finish
.finish:
        pop r13
        pop r12
        ret
