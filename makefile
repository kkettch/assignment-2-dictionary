ASM=nasm
ASMFLAGS=-f
ASMVERSION = elf64
PYTHON = python
RM = rm -f
%.o: %.asm 
	$(ASM) $(ASMFLAGS) $(ASMVERSION) -o $@ $< 
main.o: main.asm
	$(ASM) $(ASMFLAGS) $(ASMVERSION) -o $@ $< 

program: main.o lib.o dict.o
	ld -o $@ $^
clean:
	$(RM) *.o program
test:
	$(PYTHON) test.py
